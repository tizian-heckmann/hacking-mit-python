from sitename import app, db
from flask import render_template
from sqlalchemy import text
import json

@app.route("/", methods=["GET"])
def base():
    return render_template("base.html")

@app.route("/home", methods=["GET"])
def home():
    return render_template("home.html")

@app.route("/get_db", methods=["GET"])
def get_db():
    query = f"SELECT * FROM users;"
    resultset = db.session.execute(text(query))
    results = resultset.fetchall()
    results = [tuple(row) for row in results]
    json_result = json.dumps(results)
    return json_result
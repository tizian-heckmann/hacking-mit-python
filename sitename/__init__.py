from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from dotenv import dotenv_values
import os

app = Flask(__name__)
config = dotenv_values(".env")
DATABASE_PASSWORD = config["DATABASE_PASSWORD"]
DATABASE_USER = config["DATABASE_USER"]

app.config["SQLALCHEMY_DATABASE_URI"] = "postgresql://" + DATABASE_USER + ":" + DATABASE_PASSWORD + "@theckmann.ddns.net:5433/hackpy"
db = SQLAlchemy(app)

from sitename import routes